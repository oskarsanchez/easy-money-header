class EasyMoneyHeader extends Polymer.Element {

  static get is() {
    return 'easy-money-header';
  }

  static get properties() {
    return {
      user : {
        type: Object,
        value: {}
      },
      title: {
        type: String
      },
      back: {
        type: String,
        value: false
      },
      loading:{
        type: Boolean,
        notify: true,
        value: false
      }
    };
  }
  ready (){
    super.ready();
  }
  _handleRequestLogoutSuccess(evt) {
    const detail = evt.detail;
    this.loading = false;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    sessionStorage.setItem("token","");
    sessionStorage.setItem("userId","");

    this.dispatchEvent(new CustomEvent('logout-user',
        {bubbles: true, composed: true, detail: { msg: msg }}));
  }

  _handleRequestLogoutError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg;
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('logout-user-error', {
      bubbles:true,
      composed:true,
      detail: {
            msg: msg
      }
    }));
  }

  _handleLogoutButtonPressed(evt){
    this.loading = true;
    this.$.logoutdp.host = cells.urlEasyMoneyBankAuthServices;
    this.$.logoutdp.generateRequest();
  }

  _handleEditUserButtonPressed(evt){
    this.dispatchEvent(new CustomEvent('edit-user',{bubbles:true, composed:true}));
  }
  _handleBackButtonPressed(evt){
    this.dispatchEvent(new CustomEvent('back',{bubbles:true, composed:true}));
  }
}
customElements.define(EasyMoneyHeader.is, EasyMoneyHeader);
